//
//  Ship.h
//  HelloObjective-C
//
//  Created by Admin on 30/09/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"

@interface Ship : NSObject

-(Person *)captain;
-(void)setCaptain:(Person *)theCaptain;

+(Ship *)shipWithCaptain:(Person *)theCaptain;

@end
