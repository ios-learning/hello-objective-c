//
//  main.m
//  HelloObjective-C
//
//  Created by Admin on 30/09/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"
#import "Ship.h"

typedef struct {
    float x;
    float y;
} Point2D;

NSInteger sortFunction(id item1, id item2, void *context) {
    float number1 = [item1 floatValue];
    float number2 = [item2 floatValue];
    if (number1 < number2) {
        return NSOrderedAscending;
    } else if (number1 > number2) {
        return NSOrderedDescending;
    } else {
        return NSOrderedSame;
    }
}

// required unsetting ARC in the project's settings
void memoryManagement() {
    // Claim the object
    Person *frank = [[Person alloc] init];
    
    // Use the object.
    frank.name = @"Frank";
    NSLog(@"%@", frank.name);
    
    // Free the object.
    [frank release];
    
    Person *samantha = [[Person alloc] init];
    Ship *discoveryOne = [[Ship alloc] init];
    samantha.name = @"Sam";
    [discoveryOne setCaptain:samantha];
    NSLog(@"%@", [discoveryOne captain].name);
    
    [samantha release];
    
    NSLog(@"%@", [discoveryOne captain].name);
    
    [discoveryOne release];
}

void properties() {
    Person *frank = [[Person alloc] init];
    [frank setName:@"Frank"];
    [frank setEmployed:YES];
    if ([frank isEmployed]) {
        NSLog(@"Frank is employed");
    } else {
        NSLog(@"Frank is unemployed");
    }
    
    // Dot Syntax
    
    Person *anna = [[Person alloc] init];
    anna.name = @"Anna";
    NSLog(@"%@", anna.name);
}

void foundationDataStructures() {
    
    
    // NSNumber
    int someInteger = 25;
    NSNumber *someNumber = [NSNumber numberWithInt:someInteger];
    NSLog(@"The stored number is %i", [someNumber intValue]);
    NSLog(@"The stored number is %@", someNumber);
    
    
    // NSDecimalNumber
    NSDecimalNumber *someDecimal = [NSDecimalNumber decimalNumberWithString:@"0.2"];
    NSLog(@"The stored decimal number is %@", someDecimal);
    
    NSDecimalNumber *subtotal = [NSDecimalNumber decimalNumberWithString:@"10.99"];
    NSDecimalNumber *discount = [NSDecimalNumber decimalNumberWithString:@".25"];
    NSDecimalNumber *total = [subtotal decimalNumberByMultiplyingBy:discount];
    NSLog(@"The product costs: %@", total);
    
    NSDecimalNumberHandler *roundUp = [NSDecimalNumberHandler
                                       decimalNumberHandlerWithRoundingMode:NSRoundUp
                                       scale:2
                                       raiseOnExactness:NO
                                       raiseOnOverflow:NO
                                       raiseOnUnderflow:NO
                                       raiseOnDivideByZero:YES];
    
    NSDecimalNumber *roundedTotal = [subtotal decimalNumberByMultiplyingBy:discount
                                                              withBehavior:roundUp];
    NSLog(@"The product costs: %@", roundedTotal);
    
    // Converting NSDecimalNumber to double, NSString
    double totalDouble = [roundedTotal doubleValue];
    NSString *totalAsString = [roundedTotal stringValue];
    
    // NSString
    NSString *quote = @"Open the pod bay doors, HAL.";
    for (int i=0; i< [quote length]; ++i){
        NSLog(@"%c", [quote characterAtIndex:i]);
    }
    
    NSRange range = NSMakeRange(4, 18);
    NSString *partialQuote = [quote substringWithRange:range];
    NSLog(@"%@", partialQuote);
    
    NSString *target = @"HAL.";
    NSRange result = [quote rangeOfString:target];
    NSLog(@"Found %@ at index %lu. It's %lu characters long.",
          target, result.location, result.length);
    
    // NSMutableString
    
    // First with immutable strings
    NSString *quote2 = @"I'm sorry, Dave. I'm afraid I can't do that.";
    NSString *newQuote = [quote2 stringByReplacingCharactersInRange:NSMakeRange(11, 4)
                                                         withString:@"Capt'n"];
    NSLog(@"%@", newQuote);
    
    // Now with mutable strings
    NSMutableString *mquote = [NSMutableString stringWithString:quote2];
    [mquote replaceCharactersInRange:NSMakeRange(11, 4) withString:@"Capt'n"];
    NSLog(@"%@", mquote);
    
    // NSArray
    
    NSNumber *n1 = [NSNumber numberWithFloat:22.5f];
    NSNumber *n2 = [NSNumber numberWithFloat:8.0f];
    NSNumber *n3 = [NSNumber numberWithFloat:-2.9f];
    NSNumber *n4 = [NSNumber numberWithFloat:13.1f];
    NSArray *numbers = [NSArray arrayWithObjects: n1, n2, n3, n4, nil];
    NSLog(@"%@", numbers);
    
    NSArray *sortedNumbers = [numbers sortedArrayUsingFunction:sortFunction context:NULL];
    NSLog(@"%@", sortedNumbers);
    
    // NSMutableArray
    
    // Define some people
    NSString *p1 = @"HAL";
    NSString *p2 = @"Dave";
    NSString *p3 = @"Heywood";
    
    // Initialize an empty queue
    NSMutableArray *queue = [NSMutableArray arrayWithCapacity:4];
    [queue addObject:p1];
    [queue addObject:p2];
    [queue addObject:p3];
    
    
    // Remove from the queue
    NSLog(@"Removing %@ from queue.", [queue objectAtIndex:0]);
    [queue removeObjectAtIndex:0];
    NSLog(@"Removing %@ from queue.", [queue objectAtIndex:0]);
    [queue removeObjectAtIndex:0];
    NSLog(@"Removing %@ from queue.", [queue objectAtIndex:0]);
    [queue removeObjectAtIndex:0];
    
    // NSSet
    
    NSSet *crew = [NSSet setWithObjects:@"Dave", @"Heywood", @"HAL", nil];
    for (id member in crew) {
        NSLog(@"%@", member);
    }
    
    // NSDictionary and NSMutableDictionary
    
    NSMutableDictionary *mcrew = [NSMutableDictionary
                                  dictionaryWithObjectsAndKeys:@"Dave", @"Capt'n",
                                  @"Heywood",  @"Scientist",
                                  @"Frank",  @"Engineer", nil];
    
    [mcrew setObject:@"HAL" forKey:@"Crazy computer"];
    [mcrew removeObjectForKey:@"Engineer"];
    for(id role in mcrew) {
        NSLog(@"%@: %@", role, [mcrew objectForKey:role]);
    }
    
    Class targetClass = [NSString class];
    
    id mysteryObject = [NSNumber numberWithInt:5];
    NSLog(@"%i", [mysteryObject isKindOfClass:targetClass]);
    
    mysteryObject = [NSDecimalNumber decimalNumberWithString:@"5.1"];
    NSLog(@"%i", [mysteryObject isKindOfClass:targetClass]);
    
    mysteryObject = @"5.2";
    NSLog(@"%i", [mysteryObject isKindOfClass:targetClass]);

    
}

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        Person *somePerson = [[Person alloc] init];
        [somePerson setName:@"HAL 9000"];
        [somePerson sayHelloToName:@"Alex"];
        
        // Primitive Data Structures
        
        Point2D point1 = {10.0f, 0.5f};
        NSLog(@"The point is at (%.1f, %.1f)", point1.x, point1.y);
        
        int someValues[5] = {15, 32, 49, 90, 14};
        for (int i=0; i<5; ++i){
            NSLog(@"The value at index %i is %i", i, someValues[i]);
        }
        
        int *pointer = someValues + 1;
        
        NSLog(@"The second value is %i", *pointer);
        
        
        foundationDataStructures();
        properties();
        memoryManagement();
        
    }
    return 0;
}
