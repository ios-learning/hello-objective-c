//
//  Person.h
//  HelloObjective-C
//
//  Created by Admin on 30/09/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject {
    unsigned int _age;
}

@property (getter=isEmployed) BOOL employed;
@property (copy) NSString *name;
@property unsigned int age;

- (void)sayHelloToName:(NSString *)aName;


@end
