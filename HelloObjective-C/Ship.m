//
//  Ship.m
//  HelloObjective-C
//
//  Created by Admin on 30/09/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "Ship.h"

@implementation Ship {
    Person *_captain;
}

-(Person *)captain {
    return _captain;
}

-(void)setCaptain:(Person *)theCaptain {
    [_captain autorelease];
    _captain = [theCaptain retain];
}

+(Ship *)shipWithCaptain:(Person *)theCaptain {
    Ship *theShip = [[Ship alloc] init];
    [theShip setCaptain:theCaptain];
    return [theShip autorelease];
}

@end
