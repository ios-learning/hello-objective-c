//
//  Person.m
//  HelloObjective-C
//
//  Created by Admin on 30/09/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "Person.h"

@implementation Person

@synthesize name = _name;

-(unsigned int)age {
    return _age;
}

-(void)setAge:(unsigned int)age {
    _age = age;
}

-(void)sayHelloToName:(NSString *)aName {
    NSLog(@"Hello %@, my name is %@.", aName, _name);
}
@end
